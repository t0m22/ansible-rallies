# ansible-rallies

This is an Ansible playbook for deploying a template server to host rallies.info and any other sites that use the same server setup.

## Running the playbook
Install `ansible` from pip to ensure that it is the latest stable release:
```bash
pip install --user ansible ansible-playbook
```
(Note that ansible can also be installed from your distribution's package manager but it is not guaranteed to be compatible with this playbook)

```bash
ansible-playbook -k -K -i inventory.yml playbook.yml
```

### Reference
I think I plan three servers, as follows:
 * Results and web entries, dedicated to just those tasks. 
 * Clocks and tracking, plus Richard’s other little sites. Effectively his server. 
 * Other web sites, plus mailman for BDMC. 

Things we need to put on each server:-
 * Time sync
 * Exim and connection to smtp server
 * ufw or other firewall
 * Authorised keys
 * Logwatch
 * Cron-apt to show updates
 * Apache
 * MariaDB
 * php-fpm
 * WireGuard
I’m sure there is more, but I can’t think of it for now. 
